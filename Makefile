MOD_DIR=modules/texts/ztext/czebkr
MODULES=$(MOD_DIR)/nt $(MOD_DIR)/nt.vss $(MOD_DIR)/ot $(MOD_DIR)/ot.vss
#OSIS2MOD=/home/matej/archiv/knihovna/repos/sword/utilities/osis2mod
OSIS2MOD=osis2mod
USFM2OSIS=usfm2osis

all: module

$(MODULES): CzeBKR-OSIS.xml
	mkdir -p $(MOD_DIR)
	$(OSIS2MOD) $(MOD_DIR)  $< -v KJV -d 2 -z

CzeBKR-OSIS.xml: usfm/*
	$(USFM2OSIS) CzeBKR -v -o CzeBKR-OSIS.xml -l cze -s usfm $^

module: $(MODULES) czebkr.conf
	install -D -m 644 czebkr.conf mods.d/czebkr.conf
	zip -9vT CzeBKR.zip CzeBKR-OSIS.xml czebkr.conf

test: CzeBKR-OSIS.xml
	xmllint --noout --schema podklady-OSIS/osisCore.2.1.1.xsd CzeBKR-OSIS.xml
#	size := $(shell du -cs $(MOD_DIR)/{nt,ot}|tail -n1|cut -f1)
#	[ ${size} -ne 0 ]

download:
	(cd usfm && rm -f * && ../kralicka.py )

clean:
	rm -f *~ CzeBKR-OSIS.xml $(MODULES) CzeBKR.zip
